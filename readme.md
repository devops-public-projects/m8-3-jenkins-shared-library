
# Creating a Jenkins Shared Library


## Technologies Used
- Jenkins
- Groovy
- Docker
- Git
- Java
- Maven
- Linux (Ubuntu)


## Project Description
- Create a Jenkins Shared Library to extract common build logic
	- Create separate Git repository for Jenkins Shared Library project
	- Create functions in the JSL to use in the Jenkins pipeline
	- Integrate and use the JSL in Jenkins Pipeline (globally and for a specific project in Jenkinsfile)

## Prerequisites
- Jenkins installed on a remote server as a Docker container
- Docker Hub account
- Gitlab account and a repository
- Private Repository with the **java-maven-app** code in it. [Gitlab Link](https://gitlab.com/twn-devops-bootcamp/latest/08-jenkins/java-maven-app)

## Guide Steps
### Clone Original Repository
- `cd ~`
- `git clone https://gitlab.com/twn-devops-bootcamp/latest/08-jenkins/jenkins-shared-library/-/tree/starting-code?ref_type=heads`

### Configure Some Groovy Scripts
- Edit the downloaded repository using your preferred editor. I'll be using IntelliJ IDEA
- Create a new directory called **vars**
- Move the two functions inside of the [project directories](https://gitlab.com/twn-devops-bootcamp/latest/08-jenkins/java-maven-app) *script.groovy* file
	- Name each file as functionName.groovy
	- Paste the exact code into the file and save it

![Code moved to a Shared Library](/images/m8-3-move-code-to-shared-library.png)

### Push to new Repository
- `git init`
- `git remove add origin (Your specific GitLab project details)`
- `git add .`
- `git commit -m "Initial Commit"`
- `git push --set-upstream origin master`

![Code Successfully Uploaded to Shared Library Repository](/images/m8-3-code-pushed-to-shared-library-repo.png)

### Make Shared Library Available Globally
- Jenkins > Manage Jenkins > System > Global Pipeline Libraries > **ADD**
	- Name: **shared-jenkins-library**
	- Default Version: **Master**
		- Can change this to a hardcoded version but for ease of demonstration we'll use Master branch
	- Retrieval Method: **Modern SCM**
	- Source Code Management **Git**
	- Project Repository: **Your repository link from the previous step**
	- Credentials: Your saved Gitlab credentials
	- **Save**

### Create a Second Branch for Java-Maven-App
- From your Gitlab branch for the java-maven-app, create a new branch called **jenkins-shared-lib**
- From your computer `git checkout jenkins-shared-lib`
- We will test all changes in this branch going forward
- Open the project folder using your editor and edit the **Jenkinsfile**
- At the top add the line
	- `@Library('jenkins-shared-library')`
	- This will allow us to import our shared library into the **Jenkinsfile** and utilize the functions we added to it
- Remove the original calls for `gv.buildJar()` and `gv.buildImage()` to instead just use `buildJar()` and `buildImage()` respectively.
	- This will instead utilize our Shared Library groovy scripts instead of our **script.groovy** that is included with our project

### Run The Build
- Utilizing our previously made **my-multi-branch-pipeline** we will perform a **Scan Multibranch Pipeline Now** to get our new branch and to perform our build.

![Successful Build and Upload](/images/m8-3-successful-upload.png)

### Utilizing Parameters in Shared Library
- We can modify our `buildImage.groovy` script to utilize a parameter instead of hardcoding it.
```groovy
def call(String imageName) {
	//Line for withCredentials()
	sh "docker build -t $imageName ."
	sh 'echo $PASS | docker login -u $USER --password-stdin'
	sh "docker push $imageName"
}
```
- We can also modify our `buildJar.groovy` to include Jenkins environment variables
```groovy
def call() {
	echo "Building the appplication for branch $BRANCH_NAME"
	sh 'mvn package'
}
```
- Inside of our `Jenkinsfile` we can then give a parameter when calling `buildImage`
```groovy
script {
	buildImage "DOCKER_REPO/demo-app:jma-3.0"
}
```
- Running our build again for the `jenkins-shared-lib` pipeline gives us a success
![Successful Jenkins Env Var Used](/images/m8-3-jenkins-env-var-utilized.png)

![Successful Groovy Script Parameter](/images/m8-3-groovy-script-parameter-success.png)

### Extracting Logic into Groovy Classes
- Under `src` directory create a new directory called `com.example` 
- Under `src/com.example` create a new file called `Docker.groovy`
	- We will extract our current Docker command logic from `buildImage.groovy` into this class

```groovy
package com.example

class Docker implements Serializable {
	def script
	
	Docker(script) {
		this.script = script
	}
	//Our code previous inside of buildImage.groovy with slight modification
	def buidlDockerImage(string imageName) {
		//Line for withCredentials()
		script.sh "docker build -t $imageName ."
		script.sh "echo '${script.PASS}' | docker login -u '${script.USER}' --password-stdin"
		script.sh "docker push $imageName"
	}
}
```
- We can now call this new Docker Class inside of our previous `buildImage.groovy` file
```groovy
import com.example.Docker

def call(String imageName) {
	return new Docker(this).buildDockerImage(imageName)
}
```
- Running an updated build will now utilize this class
	- Note: Using Groovy string interpolation for passing sensitive information in Jenkins will result in an error and is not ideal practice, but for demonstration purposes this will work.

### Separating buildDockerImage into multiple functions
- We will split out the `buildDockerImage` actions into their own respective functions called `dockerBuild`, `dockerLogin`, and `dockerPush`
- Each will call its respective class function, below is an example for `dockerPush.groovy`
```groovy
//dockerPush.groovy
import com.example.Docker

def call(String imageName) {
	return new Docker(this).dockerPush(imageName)
}
```

```groovy
//Jenkinsfile changes
stage("build and push image") {
	steps {
		script {
			buildImage 'IMAGE_NAME/demo-app:jma-3.1'
			dockerLogin()
			dockerPush 'IMAGE_NAME/demo-app:jma-3.1'
		}
	}
}
```
![](/images/m8-3-separated-functions-success.png)

### Project Scoped Shared Library
- We will swap our `java-maven-app` `Jenkinsfile` code from @Library to the below code
- This will work the exact same but the library is only available in this project now
```groovy
library identifier: 'jenkins-shared-library@main', retriever: modernSCM(
	[$classs: 'GitSCMSource'.
	remote: 'SHARED_LIBRARY_GIT_URL',
	credentialsId: 'gitlab-credentials'])
```